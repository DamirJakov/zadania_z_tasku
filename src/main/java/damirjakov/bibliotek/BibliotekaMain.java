package damirjakov.bibliotek;

import java.util.Scanner;

public class BibliotekaMain {
    public static void main(String[] args) {

        Shelf shelf[] = new Shelf[10];


        Scanner inputScanner = new Scanner(System.in);
        boolean adding = true;
        while (adding) {
            System.out.println("Valid commands are:\n" +
                    "1 - Wprowadz ksiazkie,\n" +
                    "2 - podaj nr miejsca,\n" +
                    "3 - wpisz zawartosc,<\n" +
                    "0 - Wprowadz quit.\n");

            int command = inputScanner.nextInt();
            switch (command) {
                case 1: {
                    System.out.println("Podaj Miejsce:");
                    int miejsce = inputScanner.nextInt();

                    System.out.println("Podaj półki:");
                    int shelf2 = inputScanner.nextInt();

                    System.out.println("Podaj Tytul:");
                    String tytul = inputScanner.next();

                    System.out.println("Podaj Autora:");
                    String autor = inputScanner.next();

                    System.out.println("Podaj Rok Wydania:");
                    int rok = inputScanner.nextInt();

                    Book ksiazka = new Book();


                    ksiazka.setTytul(tytul);
                    ksiazka.setAutor(autor);
                    ksiazka.setRokWydania(rok);

                    shelf[shelf2].setBook(miejsce, ksiazka);

                    break;
                }
                case 2: {
                    System.out.println("Podaj Miejsce:");
                    int miejsce = inputScanner.nextInt();
                    System.out.println("Podaj półki:");
                    int shelf2 = inputScanner.nextInt();
                    Book ksiazka = new Book();
                    ksiazka = shelf[shelf2].getBook(miejsce);
                    if (ksiazka != null) {
                        System.out.println(ksiazka.getTytul());
                        System.out.println(ksiazka.getAutor());
                        System.out.println(ksiazka.getRokWydania());
                    }
                    break;
                }
                case 3: {
                    Book ksiazka = new Book();
                    for(int i = 0; i < shelf.getSize(); i++) {
                        ksiazka = shelf.getBook(i);
                        if (ksiazka != null) {
                            System.out.println(ksiazka.getTytul());
                            System.out.println(ksiazka.getAutor());
                            System.out.println(ksiazka.getRokWydania());
                        }

                    }


                }
                case 0: {
                    adding = false;
                    break;
                }
                default: {
                    System.out.println("Valid commands are:\n" +
                            "1 - Wprowadz ksiazkie,\n" +
                            "2 - podaj nr miejsca,\n" +
                            "3 - wpisz zawartosc,<\n" +
                            "0 - Wprowadz quit.\n");
                }
            }
        }


    }
}

