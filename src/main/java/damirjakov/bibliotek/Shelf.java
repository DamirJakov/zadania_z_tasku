package damirjakov.bibliotek;

public class Shelf {

    private Book[] ksiazki = new Book[10];

    public Book getBook(int index) {
        return ksiazki[index];
    }

    public void setBook(int index, Book ksiazka) {
        ksiazki[index] = ksiazka;
    }
    public int getSize(){
        return ksiazki.length;
    }
}
